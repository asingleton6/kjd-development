    <footer>
        <div class="container">
            <div class="row footer-padding">
                <div class="col-lg-2">
                    <nav class="nav footer-nav">
                        <h5>SITE</h5>
                        <ul>
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="about.php">About</a>
                            </li>
                            <li>
                                <a href="about.php">Testimonial</a>
                            </li>
                            <li>
                                <a href="about.php">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-2">
                    <nav class="nav footer-nav">
                        <h5>COMPANY</h5>
                        <ul>
                            <li>
                                <a href="index.php">Commercial</a>
                            </li>
                            <li>
                                <a href="about.php">Residential</a>
                            </li>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-2"></div>
                <div class="col-lg-6 text-right">
                    <p>Copyright &copy; KJD Development 2017</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</php>
