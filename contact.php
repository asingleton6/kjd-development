<?php include_once("header.php");?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 about-img">
                <img src="img/contact.jpeg">
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <h1 class="about-header">Contact Us</h1>
                    <hr>
                </div>
                <div class="col-md-2">
                    <p>1234 Melrose Place<br>Atlanta, GA 30303</p>
                    <p>123.456.7890</p>
                    <p><a href="mailto:name@example.com">name@example.com</a></p>
                </div>
                <div class="col-md-5">
                    <form role="form">
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <input type="text" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group col-lg-12">
                                <input type="email" class="form-control" placeholder="Email Address">
                            </div>
                            <div class="form-group col-lg-12">
                                <input type="tel" class="form-control" placeholder="Phone Number">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-12">
                                <textarea class="form-control" placeholder="Message" rows="6"></textarea>
                            </div>
                            <div class="form-group col-lg-12">
                                <input type="hidden" name="save" value="contact">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
                    <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;ll=33.779290,-84.380666&amp;spn=33.779290,-84.380666&amp;t=m&amp;z=10&amp;output=embed"></iframe>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /.container -->
<?php include_once("footer.php");?>
