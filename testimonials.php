<?php include_once("header.php");?>

    <div class="container">

        <div class="row">
            <div class="col-lg-12 about-img">
                <img src="img/testimonial.jpeg">
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12 justify">
                    <h3 class="about-header">Testimonials</h3>
                    <hr>
                    <div class="row">
                       <div class="col-lg-8">
                           <p><strong>"</strong>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.<strong>"</strong></p>
                           <h4> - Bob Smith</h4>
                           <p>Atlanta</p>
                        </div>
                        <div class="col-lg-4">
                            <img src="http://placehold.it/350x250/222222">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
                    <h5> - Bob Smith</h5>
                    <p>Atlanta</p>
                </div>
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</p>
                    <h5> - Bob Smith</h5>
                    <p>Atlanta</p>
                </div>
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>
                    <h5> - Bob Smith</h5>
                    <p>Atlanta</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
<?php include_once("footer.php");?>
