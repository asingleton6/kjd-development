<?php include_once("header.php");?>

    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <div id="carousel-example-generic" class="carousel slide">
                    <!-- Indicators -->
                    <ol class="carousel-indicators hidden-xs">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="img-responsive img-full" src="img/con4.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive img-full" src="img/con2.jpeg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive img-full" src="img/con3.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive img-full" src="img/con1.jpeg" alt="">
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="icon-prev"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="icon-next"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-3">
                    <h2 class="intro-text">Service</h2>
                    <hr>
                    <img class="img-responsive img-border" src="http://placehold.it/500x300/222222" alt="">
                    <hr class="visible-xs">
                    <p><strong>At vero eos</strong> et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. </p>
                </div>
                <div class="col-lg-3">
                    <h2 class="intro-text">Service</h2>
                    <hr>
                    <img class="img-responsive img-border" src="http://placehold.it/500x300/222222" alt="">
                    <hr class="visible-xs">
                    <p><strong>At vero eos</strong> et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. </p>
                </div>
                <div class="col-lg-3">
                    <h2 class="intro-text">Service</h2>
                    <hr>
                    <img class="img-responsive img-border" src="http://placehold.it/500x300/222222" alt="">
                    <hr class="visible-xs">
                    <p><strong>At vero eos</strong> et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. </p>
                </div>
                <div class="col-lg-3">
                    <h2 class="intro-text">Service</h2>
                    <hr>
                    <img class="img-responsive img-border" src="http://placehold.it/500x300/222222" alt="">
                    <hr class="visible-xs">
                    <p><strong>At vero eos</strong> et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="col-lg-3">
                    <h1>Testimonials</h1>
                    <hr class="">
                </div>
                <div class="col-lg-9"></div>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="col-lg-2">
                    <img class="img-circle" src="http://placehold.it/150x150/444444?text=HS">
                </div>
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
                    <strong>- Heather Strong</strong>
                </div>
                <div class="col-lg-2">
                    <img class="img-circle" src="http://placehold.it/150x150/444444?text=JS">
                </div>
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
                    <strong>- John Smith</strong>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="col-lg-2">
                    <img class="img-circle" src="http://placehold.it/150x150/444444?text=LB">
                </div>
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
                    <strong>- Larry Bone</strong>
                </div>
                <div class="col-lg-2">
                    <img class="img-circle" src="http://placehold.it/150x150/444444?text=PL">
                </div>
                <div class="col-lg-4">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
                    <strong>- Paula Line</strong>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
<?php include_once("footer.php");?>
