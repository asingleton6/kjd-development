<?php include_once("header.php");?>

    <div class="container">

        <div class="row">
            <div class="col-lg-12 about-img">
                <img src="img/construction.jpeg">
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-3">
                    <h3>Services</h3>
                    <hr>
                    <ul class="sidebar">
                        <li>Facilities Services</li>
                        <li>Property Management</li>
                        <li>Mechanical Services (HVAC)</li>
                        <li>Fire, CCTV, and Alarm Systems</li>
                        <li>Energy Management Services</li>
                        <li>Parking Lot Management</li>
                    </ul>
                </div>
                <div class="col-lg-9 justify">
                    <h3 class="about-header">Commercial Sector</h3>
                    <hr>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
<?php include_once("footer.php");?>
